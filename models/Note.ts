import { Schema, model, Document } from 'mongoose';

export interface Note extends Document {
  userId: string;
  completed: boolean;
  text: string;
  createdDate: Date;
}

const NoteSchema: Schema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  completed: {
    type: Boolean,
    default: false,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },

});

export default model<Note>('Note', NoteSchema);
