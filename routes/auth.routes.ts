import * as express from 'express';
import { authValidate } from '../middleware/validate.middleware';
import { register, login } from '../controllers/auth.controller';

const router = express.Router();

// api/auth
router.post('/register', authValidate, register);

router.post('/login', authValidate, login);

module.exports = router;
