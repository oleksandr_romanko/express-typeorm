import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { RequestHandler, Request, Response } from 'express';
import UserModel from '../models/User';

const register: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { username, password } = req.body;
    const candidate = await UserModel.findOne({ username });
    if (candidate) {
      return res.status(400).json({ message: 'User has already exists' });
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new UserModel({ username, password: hashedPassword });
    await user.save();
    return res.json({ message: 'User has been registered succesfully' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const login: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { username, password } = req.body;
    const user = await UserModel.findOne({ username });
    if (!user) {
      return res.status(400).json({ message: 'User has no found' });
    }
    const isMatched = await bcrypt.compare(password, user.password);
    if (!isMatched) {
      return res.status(400).json({ message: 'Wrong password' });
    }
    const token = jwt.sign(
      { username, userId: user.id },
      config.get('jwt_secret'),
      { expiresIn: '1h' },
    );
    return res.json({ message: 'Succses', jwt_token: token });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

export { register, login };
