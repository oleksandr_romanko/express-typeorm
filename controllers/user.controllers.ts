import * as bcrypt from 'bcryptjs';
import { RequestHandler, Request, Response } from 'express-serve-static-core';
import UserModel from '../models/User';

const getProfile: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { username } = req.body.user;
    const user = await UserModel.findOne({ username });
    if (!user) {
      return res.status(400).json({ message: 'User has no found' });
    }
    return res.json({ user });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const deleteProfile: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { username } = req.body.user;
    const user = await UserModel.findOneAndRemove({ username });
    if (!user) {
      return res.status(400).json({ message: 'User has no found' });
    }
    return res.json({ message: 'Succses' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const changePassword: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { oldPassword, newPassword } = req.body;
    const { username } = req.body.user;
    const user = await UserModel.findOne({ username });
    if (!user) {
      return res.status(400).json({ message: 'User has no found' });
    }
    const isMatched = await bcrypt.compare(oldPassword, user.password);
    if (!isMatched) {
      return res.status(400).json({ message: 'The old password has no match' });
    }
    const hashedPassword = await bcrypt.hash(newPassword, 12);
    await UserModel.findOneAndUpdate({ username }, { password: hashedPassword });
    return res.json({ message: 'Succses' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

export { getProfile, deleteProfile, changePassword };
