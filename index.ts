import * as express from 'express';
import * as morgan from 'morgan';
import * as config from 'config';
import * as mongoose from 'mongoose';
import * as path from 'path';
import * as rfs from 'rotating-file-stream';

const authRoutes = require('./routes/auth.routes');
const userRoutes = require('./routes/user.routes');
const noteRoutes = require('./routes/note.routes');

const app = express();

app.use(express.static(path.resolve(__dirname, 'client')));

const accessLogStream = rfs.createStream('access.log', {
  interval: '1d',
  path: path.join(__dirname, 'log'),
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('common', { stream: accessLogStream }));
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/notes', noteRoutes);

const PORT = config.get('port') || 8080;

const start = async () => {
  try {
    await mongoose.connect(config.get('mongoUri'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
    });
  } catch (e) {
    process.exit(1);
  }
};

start();
