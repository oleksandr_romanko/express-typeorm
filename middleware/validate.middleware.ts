import * as Joi from 'joi';
import { RequestHandler, Request, Response, NextFunction } from 'express';

const authValidate: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  const schema = Joi.object({
    username: Joi.string()
      .required(),
    password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
      .required(),
  });

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const userPatchValidate: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    newPassword: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

export { authValidate, userPatchValidate };
